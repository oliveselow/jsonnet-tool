package render

// Options are the options for rendering files
type Options struct {
	MultiDir       string
	FilenamePrefix string
	Header         string
	PriorityKeys   []string
}
