module gitlab.com/gitlab-com/gl-infra/jsonnet-tool

go 1.16

require (
	github.com/fatih/color v1.9.0
	github.com/google/go-jsonnet v0.17.0
	github.com/spf13/cobra v1.2.1
	gopkg.in/yaml.v2 v2.4.0
)
